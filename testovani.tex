\chapter{Testování} \label{ch:testovani}
Testování by mělo být součástí vývoje jakéhokoliv softwaru, tím spíše pokud se jedná o~knihovnu.
Správně napsané testy nám velmi usnadňují vývoj -- po provedení změn stačí spustit testy a ty by měly odhalit většinu chyb, které jsme mohli změnou způsobit.
V~případě knihovny mohou být testy navíc dobrou ukázkou jejího použití.
\section{Jednotkové a integrační testy}
Jednotkové testy testují určitou komponentu, například funkci nebo třídu, v~izolaci. V~našem případě využijeme jednotkové testy například k~otestování třídy \verb|QueryBuilder|,
která poskytuje funkce pro vytváření a validaci dotazů do databáze. Tato třída nezávisí na žádné jiné a jednoduše tak můžeme otestovat její správnou funkci voláním jejích metod
se zvolenými argumenty a porovnáním návratové hodnoty s~očekávaným výsledkem.

Integrační testy testují spolupráci komponent. V~našem případě je využijeme zejména k~otestování ukládání, mazání a čtení z~databáze, vytváření databází a tabulek a 
nastavení integritních omezení.
\subsection{Typy testů}
Na androidu existují dva typy testů, \emph{Local tests} a \emph{Instrumented tests}.

\emph{Local tests} jsou testy, které nepotřebují přístup k~Android \acrshort{API}.
Jsou prováděny v~lokální \acrshort{JVM} a umisťují se do adresáře \verb|app/src/test/java|.

\emph{Instrumented tests} mají přístup k~Android \acrshort{API}.
Jsou prováděny na fyzickém zařízení s~\acrshort{OS} Android, případně v~emulátoru, a umisťují se
do adresáře \verb|app/src/androidTest/java|. \cite{tests1}

\subsection{Testovací framework}
Pro jednotkové testy se na Androidu používá framework \emph{JUnit} \cite{tests_junit}.
Android s~ním spolupracuje pomocí knihovny \emph{Testing Support Library} \cite{tests_supportlib}, která obsahuje mimo jiné
třídu \verb|AndroidJUnitRunner|. Ta nám umožňuje spouštět testy JUnit verze 3 a 4 na Android zařízeních. 
Zajišťuje nahrání testů do zařízení, jejich vykonání a zobrazení výsledků. Její součástí je také třída
\verb|InstrumentationRegistry|, která zajišťuje přístup k~informacím o~běhu testu. Můžeme z~ní získat
například \verb|Context| naší aplikace. \verb|AndroidJUnitRunner| nám dále umožňuje filtrovat testy na základě různých kritérií,
k~čemuž využívá následující anotace:
\begin{itemize}
    \item \verb|@RequiresDevice| -- test se nespustí na emulátoru, jen na fyzickém zařízení
    \item \verb|@SDKSupress(minSdkVersion = 18)| -- test se nespustí na zařízení, které má \acrshort{API} level menší než
		specifikuje parametr \verb|minSdkVersion|
    \item \verb|@SmallTest| -- test by neměl trvat déle než 200 milisekund
    \item \verb|@MediumTest| -- test by neměl trvat déle než jednu sekundu
    \item \verb|@LargeTest| -- test trvající déle než jednu sekundu
\end{itemize}
Anotace \verb|@SmallTest|, \verb|@MediumTest| a \verb|@LargeTest| však dobu běhu daného testu nevynucují, jak by se mohlo zdát.
Slouží spíše k~rozdělení testů do tří skupin, vývojář má následně možnost spustit jen některou z~nich.


Samotný JUnit verze 4 využívá také řadu anotací, k~nejdůležitějším patří:
\begin{itemize}
    \item \verb|@RunWith(AndroidJUnit4.class)| -- určuje třídu, která bude testy vykonávat, v~našem případě to bude \verb|AndroidJUnit4.class|
		pro testy, které potřebují Android \acrshort{API} a \verb|JUnit4.class| pro ty ostatní
    \item \verb|@Test| -- touto anotací musí být označena každá metoda, která se má v~rámci testování spouštět
    \item \verb|@Before| -- metoda označená touto anotací se provede před každou testovací metodou
    \item \verb|@After| -- metoda označená touto anotací se provede po každé testovací metodě
\end{itemize}

V~ukázce \ref{code:junittests} je příklad testovací třídy. Můžeme zde mimo jiné vidět použití metod označených
anotacemi \verb|@Before| a \verb|@After|, které zajišťují vytvoření stejného počátečního prostředí pro každý test.
Dále je zde použita třída \verb|InstrumentationRegistry| pro získání \emph{Contextu}, který potřebujeme k~inicializaci
naší knihovny.

\begin{listing}[htbp]
\caption{JUnit test, který poběží na zařízení s~OS Android}
\label{code:junittests}
\begin{minted}{java}
@RunWith(AndroidJUnit4.class)
public abstract class TestClass {

    @Before
    public void before() {
        Context context = InstrumentationRegistry.getTargetContext();
	Joogar.init(context);
    }
    
    @Test
    public void standardTest() {...}
    
    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.HONEYCOMB)
    public void testWithFilter() {...}

    @After
    public void after() {
	List<JoogarDatabase> databases = Joogar.getInstance().getDatabases();

        for (JoogarDatabase database : databases) {
            database.close();
            database.getPath().delete();
        }

        Joogar.getInstance().close();
    }
}
\end{minted}
\end{listing}

 
\section{Výkonové testy} \label{ch:perftests}
Pro porovnání výkonnosti jsme využili projekt \emph{Android ORM benchmark updated} \cite{orm_benchmark},
který porovnává rychlosti čtení a zapisování několika \acrshort{ORM} knihoven. K~porovnávaným knihovnám navíc
přidává maximálně optimalizované řešení využívající pouze Android \acrshort{API}.
Námi byla přidána původní verze knihovny Joogar a verze vytvořená v~této práci.
Testy byly vykonávány na zařízení LG Nexus 5X s~OS Android verze 7.1.2 (API level 25).

\subsection{Test rychlosti zápisu}
Zápis probíhal v~jediné transakci, zapsáno bylo $2\;000$ objektů typu \verb|User| a $20\;000$ objektů typu \verb|Message|. Každý objekt typu \verb|User| obsahoval
primární klíč typu \verb|long| a dvě proměnné typu \verb|String|, které obsahovaly řetězce o~délce deseti znaků. Objekty typu \verb|Message| obsahovaly
primární klíč typu \verb|long|, jednu proměnnou typu \verb|String| s~řetězcem délky 100 znaků a dále čtyři proměnné typu \verb|long|, jednu proměnnou typu \verb|double|
a jednu proměnnou typu \verb|int|, které byly naplněny náhodnými hodnotami.

Test proběhl celkem třikrát, výsledná hodnota je průměrem těchto tří měření. Výsledky jsou zaznamenány v~grafu \ref{graph:write_speed}.
V~grafu není uveden výsledek původní verze knihovny Joogar. Jeho hodnota byla $11\;954$~milisekund, což více než třikrát převyšuje
jinak nejvyšší hodnotu v~testu. Jejím uvedením by tak nebyl dostatečně patrný rozdíl mezi ostatními knihovnami.
\begin{figure}[htbp]
\small
\begin{tikzpicture}
  \begin{axis}[
    cycle list name=exotic,
    /pgf/number format/.cd,
    use comma,
    1000 sep={\,},
    xbar, xmin=0, xmax=4000,
    enlarge y limits=0.15,
    width=12cm, height=8cm,
    xlabel={čas [ms]},
    symbolic y coords={ORMLite,DBFlow,Squeaky,Joogar 2,GreenDAO,SQLite},
    ytick=data,
    nodes near coords, nodes near coords align=horizontal,
    scaled x ticks=base 10:-3
    ]
    \addplot[color=graph!50!black, fill=graph, text=black] coordinates {
       (1759,SQLite) (1780,GreenDAO) (1841,Joogar 2)        
       (1963,Squeaky) (1970,DBFlow) 
       (3215,ORMLite)  };
  \end{axis}
\end{tikzpicture}
\caption{Srovnání rychlostí zápisu}
\label{graph:write_speed}
\end{figure}

\subsection{Test rychlosti čtení}
Čteno bylo všech $20\;000$ záznamů typu \verb|Message| zapsaných v~přechozím testu, započítaval se i čas konstrukce všech objektů.

Výsledná hodnota byla opět určena jako průměr tří měření. Výsledky jsou zaznamenány v~grafu \ref{graph:read_speed}.

V~grafu opět není uvedena původní verze knihovny Joogar, její výsledek v~tomto testu byl $5\;660$~milisekund.
\begin{figure}[htbp]
\small
\begin{tikzpicture}
  \begin{axis}[
    cycle list name=exotic,
    /pgf/number format/.cd,
    use comma,
    1000 sep={\,},
    xbar, xmin=0, xmax=2000,
    enlarge y limits=0.15,
    width=12cm, height=8cm,
    xlabel={čas [ms]},
    symbolic y coords={ORMLite,DBFlow,Joogar 2,GreenDAO,Squeaky,SQLite},
    ytick=data,
    nodes near coords, nodes near coords align=horizontal,
    scaled x ticks=base 10:-3
    ]
    \addplot[color=graph!50!black, fill=graph, text=black] coordinates {
       (810,SQLite) (896,GreenDAO) (909,Joogar 2)        
       (828,Squeaky) (1132,DBFlow) 
       (1697,ORMLite)};
  \end{axis}
\end{tikzpicture}
\caption{Srovnání rychlostí čtení}
\label{graph:read_speed}
\end{figure}

\subsection{Závěr}
Není překvapením, že v~obou testech s~mírným náskokem zvítězilo řešení, které přímo využívá Android \acrshort{API}
(v~grafech uvedeno jako \emph{SQLite}). Všechna ostatní řešení totiž musí ve výsledku použít stejný nebo velmi
podobný způsob řešení, navíc však přidají nějakou režii.

Mezi knihovnami, které využívají generování kódu (GreenDAO, Joogar 2, Squeaky, DBFlow), jsou s~výjimkou DBFlow jen
nepatrné rozdíly. Knihovna DBFlow je z~této skupiny knihoven v~obou testech nejpomalejší.

Na posledních místech v~obou testech skončily s~výrazným odstupem knihovny fungující na principu reflexe.
Předposlední místo patří knihovně ORMLite, poslední pak původní verzi knihovny Joogar.