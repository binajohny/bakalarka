\section{GreenDAO}
GreenDAO \cite{greendao_lib} je dle statistik druhá nejrozšířenější knihovna, která ukládá data do relační databáze.
Dle testů je navíc nejrychlejší (viz kapitola \ref{ch:perftests}).
Funguje na principu generování kódu, k~čemuž ovšem nevyužívá anotační procesor, ale vlastní plugin pro build systém gradle.

\subsection{Inicializace}
Pro inicializaci není potřeba vytvářet žádné třídy, vše potřebné je vygenerováno knihovnou.
Inicializace probíhá způsobem, který je vidět v~ukázce \ref{code:greendao_init}, a bude se typicky nacházet
v~\verb|onCreate| metodě naší třídy \verb|Application|. 
\subsection{Nároky na model}
Třídy, které chceme ukládat do databáze musíme označit anotací \verb|@Entity|. Primární klíč
může být tvořen jen jedním sloupečkem a musí být typu \verb|long|, značí se anotací \verb|@Id|. Jednotlivé proměnné nemusí být nijak označeny
a budou do databáze ukládány automaticky. Tomu můžeme zabránit pomocí anotace \verb|@Transient|.
GreenDAO neumí k~proměnným přistupovat přímo, nezávisle na jejich přístupnosti. Musíme pro ně tedy vytvořit gettery a settery,
případně je knihovna vygeneruje za nás. GreenDAO nám také vygeneruje kostruktor se všemi proměnnými ukládanými do databáze, pokud to
explicitně nezakážeme (potom ale musíme poskytnout konstruktor bezparametrický). Konstruktor i gettery a settery musí mít přístupnost alespoň \emph{package-private}.
Příklad takové třídy je v~ukázce \ref{code:greendao_entity}.
\subsection{API pro čtení a zápis}
Ke čtení a zapisování do databáze se využívají objekty typu \verb|*Dao|, které knihovna vygeneruje pro každou tabulku. Získání
instance třídy \verb|UserDao| je vidět v~ukázce \ref{code:greendao_init}.

Pro ukládání jsou k~dispozici následující metody:
\begin{minted}[fontsize=\footnotesize]{java}
long insert(T entity);
long insertOrReplace(T entity);
void save(T entity);
\end{minted}
Metoda \verb|insert| uloží objekt do databáze, pokud by tím došlo k~porušení integritního omezení, 
bude vyvolána výjimka. Metoda \verb|insertOrReplace| naproti tomu při konfliktu nahradí data konfliktního řádku předaným objektem.
Metoda \verb|save| objekt vloží do databáze, pokud má proměnná reprezentující jeho primární klíč výchozí hodnotu (null nebo 0). V~takovém případě
zavolá metodu \verb|insert|, v~opečném případě metodu \verb|update|. Výhodou je, že je to o~něco rychlejší než použití metody \verb|insertOrReplace|.
Nevýhodou pak je, že pokud má objekt primární klíč sice nastavený, ale na hodnotu která v~databázi ještě není uložená, neprovede se nic.
Všechny uvedené metody jsou navíc k~dispozici ve variantách pro uložení více objektů najednou, v~takovém případě je to automaticky provedeno v~transakci.

Pro upravování je k~dispozici následující metoda:
\begin{minted}[fontsize=\footnotesize]{java}
void update(T entity);
\end{minted}
Metoda \verb|update| upraví řádek v~tabulce podle odpovídajícího primárního klíče.
Je k~dispozici také varianta pro upravení více objektů najednou v~transakci.

Pro mazání jsou k~dispozici následující metody:
\begin{minted}[fontsize=\footnotesize]{java}
void deleteAll();
void delete(T entity);
void deleteByKey(K key);
\end{minted}
Funkce všech tří metod je zřejmá. Metody \verb|delete| a \verb|deleteByKey| jsou opět k~dispozici i ve variantách pro smazání více záznamů
najednou, které probíhá v~transakci. Mazat na základě specifického výběru nám umožňuje třída \verb|QueryBuilder|, o~té více později.

\pagebreak
Pro čtení dat jsou k~dispozici následující metody:
\begin{minted}[fontsize=\footnotesize]{java}
long count();
T load(K key);
List<T> loadAll();
List<T> queryRaw(String where, String... selectionArg);
\end{minted}
Funkce všech metod je zřejmá.

Pro pokročilé dotazy slouží třída \verb|QueryBuilder|. Její použití je vidět v~ukázce
\ref{code:greendao_query}. Výsledek takto vytvořeného dotazu můžeme získat ve více podobách. Jednak jako standardní \verb|List|,
ale také jako \verb|LazyList|. To je vlastní třída knihovny, která z~výsledku dotazu konstruuje objekty až v~momentě, kdy k~nim přistupujeme.
Dotaz vytvořený pomocí třídy \verb|QueryBuilder| můžeme také využít k~získání počtu vybraných řádků, nebo k~jejich smazání.

\subsection{Hodnocení}
Výhody:
\begin{itemize}
    \item Rychlost (viz kapitola \ref{ch:perftests}).
    \item Poměrně jednoduchá inicializace nevyžadující vytváření žádných tříd.
\end{itemize}
Nedostatky:
\begin{itemize}
    \item Funkce \verb|update| a \verb|delete| nemají návratovou hodnotu a nemůžeme tedy zjistit
	    výsledek operace. Tyto funkce by měly vracet počet upravených, respektive odstraněných řádků.
    \item Absence metody pro úpravu tabulky na základě určitých podmínek. Jediným způsobem jak tohoto
	    můžeme docílit je načtení všech objektů z~databáze, jejich požadovaná úprava a následné
	    promítnutí změn zpět do databáze pomocí metody \verb|update|.
    \item Metody \verb|loadAll| a \verb|queryRaw| vrací výsledek
	    v~podobě třídy \verb|List|. To znamená, že všechny objekty musí být zkonstruovány před tím, než obdržíme výsledek. To je časově
	    náročné a ne vždy je to potřeba.
    \item Způsob vytváření klauzule where při použití třídy \verb|QueryBuilder|.
	    Nemusíme sice na rozdíl od knihovny ORMLite uvádět jména sloupečků jako řetězce,
	    zápis typu \verb|UserDao.Properties.FirstName| je však celkem komplikovaný.
	    Vnořené podmínky také nejsou příliš intuitivní.
    \item Parametr porovnávací funkce při vytváření klauzule where není omezen na datový typ porovnávaného sloupečku.
    \item Nemožnost přistupovat k~proměnným přímo -- nutné vytvoření getterů a setterů.
\end{itemize}

%================================================
%		CODE
%================================================
\begin{listing}[htbp]
\caption{Inicializace databáze v~knihovně GreenDAO}
\label{code:greendao_init}
\begin{minted}{java}
DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "database");
SQLiteDatabase db = helper.getWritableDatabase();
DaoMaster daoMaster = new DaoMaster(db);
DaoSession daoSession = daoMaster.newSession();
        
UserDao userDao = daoSession.getUserDao();
\end{minted}
\end{listing}

\begin{listing}[htbp]
\caption{Třída nastavená pro použití s~knihovnou GreenDAO}
\label{code:greendao_entity}
\begin{minted}{java}
@Entity
public class User {
    @Id
    private Long id;
    private String name;
    @Transient
    private int tempUsageCount;
    
    @Generated(hash = 873297011)
    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    @Generated(hash = 586692638)
    public User() {
    }
    
    // Getters and setters
}
\end{minted}
\end{listing}


\begin{listing}[htbp]
\caption{Dotaz do databáze v~knihovně GreenDAO}
\label{code:greendao_query}
\begin{minted}{java}
QueryBuilder<User> qb = userDao.queryBuilder();
qb.where(UserDao.Properties.FirstName.eq("Joe"),
    qb.or(UserDao.Properties.YearOfBirth.gt(1970),
        qb.and(UserDao.Properties.YearOfBirth.eq(1970), UserDao.Properties.MonthOfBirth.ge(10))));
qb.limit(20).offset(5);
qb.orderAsc(UserDao.Properties.YearOfBirth);
List<User> users = qb.list();
\end{minted}
\end{listing}

\newpage