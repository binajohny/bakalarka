\section{Anotace}
Anotace nám umožňují přidávat metadata k~elementům zdrojového kódu. Mají několik různých použití, například:
\begin{itemize}
    \item Informace pro překladač -- detekce chyb, potlačení varování.
    \item Zpracování v~průběhu kompilace -- generování kódu.
    \item Zpracování za běhu aplikace.
\end{itemize}

Příklad použití anotace můžeme vidět v~ukázce \ref{code:anno2}.
Anotace může obsahovat parametry, které se uvádí v~závorkách za jejím jménem.
Pokud anotace definuje jen jeden parametr pojmenovaný
\verb!value!, nemusíme jeho jméno uvádět, jak je vidět v~ukázce \ref{code:anno3}.

\cite{anno_tut_intro, anno_tut_basics}

\begin{listing}[htbp]
\caption{Příklad použití anotace s~parametry}
\label{code:anno2}
\begin{minted}{java}
@Column(name = "jmeno", unique = true)
private String name;
\end{minted}
\end{listing}

\begin{listing}[htbp]
\caption{Příklad použití anotace s~jediným paremetrem \emph{value}}
\label{code:anno3}
\begin{minted}{java}
@SuppressWarnings("unchecked")
void myMethod() { ... }
\end{minted}
\end{listing}


\subsection{Vytváření vlastních anotací}
Pro deklaraci anotace se používá klíčové slovo \verb!@interface!. Jak může vypadat deklarace anotace je vidět
v~ukázce \ref{code:anno4}. Na deklaraci anotace můžeme rovněž aplikovat jiné anotace, kterým se říká metaanotace.
Několik metaanotací je součástí Java \acrshort{API}, pro nás budou důležité především následující dvě:
\begin{itemize}
    \label{retention}
    \item \verb!@Retention! -- určuje, kde je daná anotace dostupná. Možné hodnoty jsou:
    \begin{itemize}
        \item \verb!RetentionPolicy.SOURCE! -- dostupná pouze při kompilaci, kompilátor ji zahazuje
        \item \verb!RetentionPolicy.CLASS! -- dostupná v~bytekódu, za běhu aplikace být dostupná nemusí
        \item \verb!RetentionPolicy.RUNTIME! -- dostupná i za běhu aplikace pomocí reflexe
    \end{itemize}
    \item \verb!@Target! -- určuje, na které elementy můžeme anotaci aplikovat. Možné hodnoty jsou:
    \begin{itemize}
        \item \verb!ElementType.ANNOTATION_TYPE! -- aplikujeme na anotace
        \item \verb!ElementType.CONSTRUCTOR! -- aplikujeme na konstruktory
        \item \verb!ElementType.FIELD! -- aplikujeme na třídní a instanční proměnné
        \item \verb!ElementType.LOCAL_VARIABLE! -- aplikujeme na lokální proměnné
        \item \verb!ElementType.METHOD! -- aplikujeme na metody
        \item \verb!ElementType.PACKAGE! -- aplikujeme na balíčky
        \item \verb!ElementType.PARAMETER! -- aplikujeme na parametry metod
        \item \verb!ElementType.TYPE! -- aplikujeme na třídy, rozhraní, výčtové typy, anotace
    \end{itemize}
\end{itemize}
Jak již bylo zmíňeno dříve, anotace může obsahovat parametry. Definice dvou parametrů \verb!name!
a \verb!database! je vidět v~ukázce \ref{code:anno4}. Každému parametru můžeme určit výchozí hodnotu
pomocí klíčového slova \verb!default!. Datový typ parametru nemůže být libovolný, je omezený na:
\begin{itemize}
\tightlist
    \item primitivní datový typ,
    \item String,
    \item Class,
    \item výčtový typ,
    \item anotační typ,
    \item jednorozměrné pole kteréhokoliv z~výše uvedených.
\end{itemize}

\cite{anno_tut_declaring, anno_tut_predefined, anno_type_ele}

\begin{listing}[htbp]
\caption{Definice anotace}
\label{code:anno4}
\begin{minted}{java}
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Table {
    String name();
    String database() default "joogar";
}
\end{minted}
\end{listing}

\section{Anotační procesor}
Anotační procesor je třída, která implementuje rozhraní \verb!Processor! z~balíčku \verb|javax.annotation.processing|,
případně dědí od třídy \verb!AbstractProcessor! z~téhož balíčku.
Anotační procesor nám umožňuje zpracovávat anotace v~době kompilace.
Díky tomu můžeme generovat zdrojový kód, který bude následně zkompilován, případně analyzovat
stávající kód a na jeho základě vytvářet chyby nebo varování.

Kostru anotačního procesoru můžeme vidět v~ukázce \ref{code:anno5}.
Každý anotační procesor musí mít veřejný konstruktor bez parametrů, který bude použit kompilátorem pro vytvoření instance
procesoru. Musí také implementovat metody \verb|init|, \verb|getSupportedAnnotationTypes|, \verb|getSupportedSourceVersion|
a \verb|process|, jak je vidět ve zmíněné ukázce.

Metoda \verb|init| je volána po vytvoření instance anotačního procesoru a její parametr typu \verb|ProcessingEnvironment| nám poskytuje metody pro vytváření nových souborů,
hlášení chybových zpráv a další užitečné funkce \cite{anno_processingenv}.
Metodou \verb|getSupportedAnnotationTypes| určujeme, jaké anotace bude daný procesor zpracovávat.
Metodou \verb|getSupportedSourceVersion| určujeme nejnovější verzi jazyka Java, se kterou umí daný procesor pracovat.
Pomocí metody \verb|process| nám jsou předány elementy, které byly označeny některou z~požadovaných anotací. \cite{anno_processordoc}

Zpracování anotací probíhá v~několika kolech. 
V~každém z~nich dostane procesor ke zpracování anotace nalezené v~souborech vygenerovaných
v~kole předcházejícím. V~prvním kole to budou anotace ze všech zdrojových souborů.
Je garantováno, že pokud byl konkrétní procesor v~některém kole zavolán, bude volán i ve všech následujích kolech, nehledě na to, 
jestli přibyly nové anotace, které by mohl zpracovat.
\cite{anno_processor}
\begin{listing}[htbp]
\caption{Kostra anotačního procesoru}
\label{code:anno5}
\begin{minted}{java}
@AutoService(Processor.class)
public class JoogarProcessor extends AbstractProcessor {

    @Override
    public synchronized void init(
	ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        ...
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(
	    Table.class.getCanonicalName()
	);
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(
	Set<? extends TypeElement> annotations,
	RoundEnvironment roundEnv) {
	...
    }
}
\end{minted}
\end{listing}
