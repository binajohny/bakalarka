\section{ORMLite}
ORMLite \cite{ormlite_lib} je dle zmiňované statistiky nejpoužívanější \gls{ORM} knihovnou která používá
jako úložiště relační databázi. Není určena přímo pro Android, ale obecně pro Javu.
Funguje na principu reflexe, takže nepatří k~nejrychlejším.

\subsection{Inicializace}
Pro inicializaci musíme vytvořit třídu rozšiřující \verb|OrmLiteSqliteOpenHelper|, na jejímž pozadí se nachází \verb|SQLiteOpenHelper| z~Android \acrshort{API}.
Z~její instance následně získáme objekt typu \verb|ConnectionSource|, který potřebujeme pro vytvoření tabulek pomocí třídy \verb|TableUtils|.
Zjednodušený příklad inicializace knihovny je v~ukázce \ref{code:ormlite_init}.
\subsection{Nároky na model}
Knihovna využívá anotace. Třídy, které chceme ukládat do databáze musíme označit anotací \verb|@DatabaseTable|. Primární klíč
může být tvořen jen jedním sloupečkem, datový typ \verb|long| nebo \verb|String|. Každá proměnná, která má být ukládána, musí být označena anotací \verb|@DatabaseField|,
pomocí jejíchž parametrů můžeme dále specifikovat, jestli se jedná o~primární klíč, název sloupečku databáze a další vlastnosti. Ve výchozím stavu
jsou hodnoty čteny a nastavovány proměnným přímo, můžeme si ale vyžádat i použití getterů a setterů. Díky použití reflexe nejsou kladeny žádné
nároky na přístupnout proměnných, mohou být i \emph{private}. Vyžadován je bezparametrický konstruktor. Příklad takové třídy je v~ukázce \ref{code:ormlite_entity}.
\subsection{API pro čtení a zápis}
Pro čtení a zapisování dat do databáze potřebujeme instanci objektu \verb|Dao|. Tu získáme z~naší třídy rozšiřující \verb|OrmLiteSqliteOpenHelper|,
jak je vidět v~ukázce \ref{code:ormlite_init}.

Pro ukládání jsou k~dispozici následující metody:
\begin{minted}[fontsize=\footnotesize]{java}
int create(T data);
T createIfNotExists(T data);
CreateOrUpdateStatus createOrUpdate(T data);
\end{minted}
Metoda \verb|create| uloží objekt do databáze, přičemž nebere v~úvahu jeho primární klíč -- měl by tak vždy vzniknout nový řádek v~databázi. 
Metoda \verb|createIfNotExists| uloží objekt do databáze, ale jen pokud ta už neobsahuje
řádek se stejným primárním klíčem. Podle dokumentace to dělá tak, že nejdříve provede vyhledávání v~databázi podle primárního klíče objektu, a pokud
nic nenajde, zavolá metodu \verb|create|.
Metoda \verb|createOrUpdate| uloží objekt do databáze, pokud již v~databázi existuje, upraví daný záznam. Opět nejdříve vyhledává v~databázi podle primárního
klíče a na základě výsledku volá buď metodu \verb|create| nebo \verb|update|.

Pro upravování jsou k~dispozici následující metody:
\begin{minted}[fontsize=\footnotesize]{java}
int update(T data);
int updateId(T data, ID newId);
int update(PreparedUpdate<T> preparedUpdate);
\end{minted}
Metoda \verb|update| upraví řádek v~tabulce podle odpovídajícího primárního klíče.
Metoda \verb|updateId| navíc změní hodnotu primárního klíče na hodnotu specifikovanou parametrem \verb|newId|.
Poslední metoda s~parametrem \verb|preparedUpdate| se hodí pro upravování více řádků
tabulky najednou na základě podmínek specifikovaných pomocí třídy \verb|Where| (o~té více později).

Pro mazání jsou k~dispozici následující metody:
\begin{minted}[fontsize=\footnotesize]{java}
int delete(T data);
int deleteById(ID id);
int delete(Collection<T> datas);
int deleteIds(Collection<ID> ids);
int delete(PreparedDelete<T> preparedDelete);
\end{minted}
První čtyři nám umožňují mazat z~databáze na základě primárního klíče, který si buď zjistí z~předaného objektu,
případně jim ho předáme parametrem přímo. Poslední metoda s~parametrem \verb|preparedDelete| nám opět umožňuje mazat
na základě podmínek specifikovaných pomocí třídy \verb|Where|.

Pro čtení dat jsou k~dispozici následující metody:
\begin{minted}[fontsize=\footnotesize]{java}
long countOf();
long countOf(PreparedQuery<T> preparedQuery);
T queryForId(ID id);
T queryForSameId(T data);
T queryForFirst(PreparedQuery<T> preparedQuery);
List<T> query(PreparedQuery<T> preparedQuery);
List<T> queryForAll();
List<T> queryForEq(String fieldName, Object value);
List<T> queryForMatching(T matchObj);
List<T> queryForFieldValues(Map<String, Object> fieldValues);
\end{minted}
Funkce většiny z~nich by měla být zřejmá. Za zmínku stojí metoda \verb|queryForEq|, která hledá záznamy jejichž sloupeček specifikovaný
parametrem \verb|fieldName| má stejnou hodnotu jako parametr \verb|value|. 
Metoda \verb|queryForMatching| hledá na základě hodnot předaného objektu, ale pouze těch, které mají jinou než výchozí hodnotu -- pokud bude
tedy objekt obsahovat například proměnnou typu \verb|boolean| s~hodnotou \verb|false|, nebude tato použita.

Komplexní dotazy do databáze nám umožňuje třída \verb|QueryBuilder|. Ta navíc může vracet výsledek dotazu v~podobě třídy
\verb|ClosableIterator|, která konstruuje jednotlivé objekty až ve chvíli, kdy k~nim chceme přistoupit.
Její použití, stejně jako použití zmiňované třídy \verb|Where| je vidět v~ukázce \ref{code:ormlite_query}.

\subsection{Hodnocení}
Výhody:
\begin{itemize}
    \item Vyspělost a velké rozšíření knihovny -- dá se očekávat, že neobsahuje žádné závažné chyby.
\end{itemize}

Nedostatky:
\begin{itemize}
    \item Není specializovaná na Android, nemůže tak být zcela optimalizována pro použití na této platformě.
    \item Všechny metody pro vyhledávání s~výjimkou dotazů zkonstruovaných pomocí třídy \verb|QueryBuilder| vrací výsledek
	    v~podobě třídy \verb|List|. To znamená, že všechny objekty musí být zkonstruovány před tím, než obdržíme výsledek. To je časově
	    náročné a ne vždy je to potřeba.
    \item Použití reflexe -- rychlostí nemůže konkurovat knihovnám, které generují zdrojový kód.
    \item Relativně složitá inicializace -- nutnost vytvořit speciální třídu a každou tabulku vytvořit zvláštním příkazem.
    \item Při sestavování dotazu pomocí třídy \verb|Where| musíme uvádět jména sloupečků, což je náchylné k~chybám.
    \item Parametr porovnávací funkce třídy \verb|Where| není omezen na datový typ porovnávaného sloupečku,
	    ale může jím být jakýkoliv objekt.
\end{itemize}



%================================================
%		CODE
%================================================
\begin{listing}[htbp]
\caption{Inicializace knihovny ORMLite}
\label{code:ormlite_init}
\begin{minted}{java}
public class DataBaseHelper extends OrmLiteSqliteOpenHelper {...}

// In onCreate of our Application class
DataBaseHelper.init(context);
ConnectionSource connectionSource = DatabaseHelper.getInstance().getConnectionSource();
TableUtils.createTableIfNotExists(connectionSource, User.class);

// When we need Dao to read/write to database
Dao<User, Long> userDao = DataBaseHelper.getInstance().getDao(User.class);
\end{minted}
\end{listing}

\begin{listing}[htbp]
\caption{Třída nastavená pro použití s~knihovnou ORMLite}
\label{code:ormlite_entity}
\begin{minted}{java}
@DatabaseTable
public class User {
    @DatabaseField(id = true)
    private String name;
    @DatabaseField
    private int birthYear;
    // Won't be persisted
    private int tempUsageCount;

    public User() {
    }
}
\end{minted}
\end{listing}

\begin{listing}[htbp]
\caption{Dotaz do databáze v~knihovně ORMLite}
\label{code:ormlite_query}
\begin{minted}{java}
QueryBuilder<User, Long> qb = userDao.queryBuilder();
Where<User, Long> w = qb.where();
w.and(w.eq("first_name", "Joe"),
    w.or(w.gt("birth_year", 1970),
        w.and(w.eq("birth_year", 1970), w.ge("birth_month", 10))));
qb.limit(20).offset(5);
ClosableIterator<User> iterator = qb.iterator();
\end{minted}
\end{listing}

\newpage