\chapter{Úvod do problematiky}
\section{SQLite}
Pro ukládání strukturovaných dat se v~zařízeních s~\acrshort{OS} Android používá SQLite databáze.
Je to jednoduchá relační databáze s~otevřeným zdrojovým kódem.
Její hlavní výhodou předurčující ji k~použití na mobilních zařízeních je to, že na rozdíl od
většiny ostatních SQL databází běží přímo v~procesu aplikace a nevyžaduje zvláštní proces pro server.
SQLite přímo čte a zapisuje do obyčejných souborů na disku, přičemž celá databáze včetně indexů a pohledů
je uložena v~jediném souboru.

SQLite databáze podporuje jen omezené množství datových typů, jsou to:
\begin{itemize}
    \item \verb|NULL| -- hodnota null
    \item \verb|INTEGER| -- celé číslo, uloženo maximálně v~8 bytech
    \item \verb|REAL| -- desetinné číslo, uloženo v~8 bytech
    \item \verb|TEXT| -- textový řetězec
    \item \verb|BLOB| -- binární data
\end{itemize}
SQLite používá dynamické typování. Při vytváření databáze sice určujeme datové typy jednotlivých sloupečků,
ty slouží ale jen jako jakési doporučení, sloupeček může obsahovat jakýkoliv datový typ.
Jedinou výjimkou je sloupeček vytvořený jako \verb|INTEGER PRIMARY KEY|, do kterého můžeme ukládat jen hodnoty typu integer. \cite{sqlite_datatypes}

\cite{sqlite_about, sqlite_features}
\section{Android API pro SQLite}
V~následujících podkapitolách si popíšeme nejdůležitější části Android \acrshort{API} pro práci s~SQLite databází.
To se nachází v~balíčku \verb|android.database.SQLite|.
\subsection{Vytvoření databáze}
Pro vytváření a upravování databáze slouží třída \verb|SQLiteOpenHelper| \cite{sqliteopenhelper_doc}, kterou můžeme vidět v~ukázce \ref{code:sqliteopenhelper}.
Ta ve svém konstruktoru přijímá \verb|Context|, jméno databáze, třídu implementující rozhraní \verb|CursorFactory| pro konstrukci
Cursoru (může být \verb|null| pro výchozí chování) a verzi databáze (celé číslo větší než 0).
Obsahuje také dvě abstraktní metody, které musíme implementovat. 
Metoda \verb|onCreate| je volána po prvním vytvoření databáze a měli bychom v~ní vytvořit požadované tabulky a případně je naplnit výchozími hodnotami.
Metoda \verb|onUpgrade| je volána, pokud je verze existující databáze menší než verze předaná konstruktoru třídy \verb|SQLiteOpenHelper|. Slouží zejména
k~úpravě a odstranění existujících tabulek, případně k~vytvoření tabulek nových.
\begin{listing}[htbp]
\caption{Správa databáze pomocí třídy SQLiteOpenHelper}
\label{code:sqliteopenhelper}
\begin{minted}{java}
public class DbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Database.db";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    public void onCreate(SQLiteDatabase db) {...}
    
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {...}
}
\end{minted}
\end{listing}

\subsection{Upravování a čtení dat z~databáze} \label{ch:dbreadwrite}
K~upravování a čtení dat z~databáze slouží třída \verb|SQLiteDatabase| \cite{sqlitedatabase_doc}, jejíž instanci získáme z~instance
třídy \verb|SQLiteOpenHelper| voláním jedné z~metod \verb|getWriteableDatabase|, \verb|getReadableDatabase|. 
Předpisy vybraných metod pro úpravu a čtení dat můžeme vidět v~ukázce \ref{code:sqliteupdatemethods}.

Metoda \verb|execSQL| slouží k~provedení libovolného SQL příkazu, nehodí se však pro příkazy vracející nějaká data (SELECT, INSERT, UPDATE, DELETE).
Vhodná je například pro příkazy vytvářející a upravující tabulku.

Metodu \verb|insert| použijeme k~vložení řádku do tabulky. Pro reprezentaci sloupečků a jejich hodnot se
používá třída \verb|ContentValues| (viz kapitola \ref{subsection:contentvalues}). Metoda vrací \verb|id| vloženého řádku,
případně hodnotu $-1$, pokud vložení neproběhlo.

Metoda \verb|update| slouží k~úpravě existujících řádků, které splňují podmínku danou parametry
\verb|whereClause| a \verb|whereArgs|. Vrací počet upravených řádků.

Metoda \verb|delete| smaže řádky vyhovující podmínkám a vrátí jejich počet.

Metody \verb|rawQuery| a \verb|query| v~databázi najdou řádky odpovídající parametrům, které jim předáme
a vrátí nám je v~podobě \verb|Cursoru| (viz kapitola \ref{subsection:cursor}). Zatímco metoda \verb|rawQuery|
přijímá rovnou celý SQL příkaz, metoda \verb|query| přijímá odděleně jeho jednotlivé části a příkaz z~nich sestaví za nás.

Parametr \verb|whereArgs|, vyskytující se v~některých metodách doplňuje parametr \verb|whereClause| a umožňuje nám v~dotazu použít
otazníky namísto hodnot. Ty budou později nahrazeny hodnotami z~parametru \verb|whereArgs|, přičemž budou automaticky
ošetřeny znaky, které mají v~SQL příkazech zvláštní význam. Příklad takového použití můžeme vidět v~ukázce \ref{code:sqliteupdate}.

Pro často používané příkazy se hodí třída \verb|SQLiteStatement|. Ta reprezentuje předpřipravený příkaz, který stačí jen naplnit aktuálními
hodnotami a spustit. Tento příkaz můžeme využít k~ukládání, upravování a mazání dat z~databáze, dále nám umožňuje přečíst z~databáze
jedinou hodnotu typu \verb|Long| nebo \verb|String|, případně vykonat libovolný SQL příkaz. Nemůžeme ho však využít ke čtení celých řádků
z~tabulky. Vytvoření a použití takového příkazu můžeme vidět v~ukázce \ref{code:sqlitestatement}.



\subsection{ContentValues} \label{subsection:contentvalues}
ContentValues \cite{contentvalues_doc} je jednoduchá třída, která drží dvojice klíč--hodnota, kde klíč je jméno sloupečku
tabulky. Používá se pro vkládání a upravování řádků tabulek (viz ukázka \ref{code:sqliteupdatemethods}).
Příklad použití můžeme vidět v~ukázce \ref{code:insertvalues}.


\subsection{Cursor} \label{subsection:cursor}
Cursor \cite{cursor_doc} je objekt reprezentující výsledek dotazu do databáze. Ukazuje na jeden řádek výsledku. Čtení dat z~databáze
probíhá až ve chvíli, kdy zavoláme metodu Cursoru, která je vyžaduje, čímž se výrazně šetří paměť.
Mezi řádky se pohybujeme pomocí metod \verb|moveToFirst()|, \verb|moveToNext()|, \verb|moveToPosition(int position)| a dalších.
Ke zjištění počtu řádků a pozice Cursoru slouží mimo jiné metody \verb|getCount()|, \verb|getPosition()|, \verb|isFirst()|,
nebo \verb|isLast()|.
Hodnoty sloupečků pak z~Cursoru čteme pomocí několika \verb|get*(int position)| metod. Ty berou jako parametr pořadové číslo sloupečku.
Pokud známe jen jméno sloupečku, můžeme jeho index zjistit pomocí metody \verb|getColumnIndex(String name)|. 
Cursor by měl být po dokončení práce uzavřen metodou \verb|close()|. 
Příklad čtení dat z~Cursoru můžeme vidět v~ukázce \ref{code:cursorread}. \cite{cursor_vogella}

\begin{listing}[htbp]
\caption{Předpisy vybraných metod pro úpravu a čtení dat z~databáze}
\label{code:sqliteupdatemethods}
\begin{minted}{java}
void execSQL(String sql);
long insert(String table, String nullColumnHack, ContentValues values);
int  update(String table, ContentValues values, String whereClause, String[] whereArgs);
int  delete(String table, String whereClause, String[] whereArgs)

Cursor rawQuery(String sql, String[] selectionArgs);
Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit);
\end{minted}
\end{listing}

\begin{listing}[htbp]
\caption{Použití metody delete}
\label{code:sqliteupdate}
\begin{minted}{java}
db.delete("table", "user_name = ?", new String[]{username});
\end{minted}
\end{listing}

\begin{listing}[htbp]
\caption{Vytvoření a použití předpřipraveného příkazu}
\label{code:sqlitestatement}
\begin{minted}{java}
SQLiteStatement statement = db.compileStatement(
	"INSERT INTO users (name, birthYear) VALUES (?, ?)");
        
statement.bindString(1, "Joe");
statement.bindLong(2, 1990);
long id = statement.executeInsert();
\end{minted}
\end{listing}

\section{Objektově-relační mapování}
V~objektově orientovaných jazycích, jakým je i Java, pracujeme s~daty ve formě objektů.
Ty mohou obsahovat proměnné nejrůznějších datových typů, zatímco relační databáze je omezena
na několik jednodušších, SQLite konkrétně na výše zmíněné čtyři.
\Gls{ORM} je technika převodu dat mezi řádky relační databáze a objekty objektově orientovaného jazyka.

\begin{listing}[htbp]
\caption{Použití ContentValues pro ukládání do databáze}
\label{code:insertvalues}
\begin{minted}{java}
ContentValues values = new ContentValues();

values.put("username", "John");
values.put("birthYear", 1990);

long id = db.insert("users", null, values);
\end{minted}
\end{listing}


\begin{listing}[htbp]
\caption{Čtení dat z~Cursoru}
\label{code:cursorread}
\begin{minted}{java}
Cursor cursor = fetchData();

if (cursor.moveToFirst()) {
    int usernameIndex = cursor.getColumnIndex("username");
    do {
	String username = cursor.getString(usernameIndex);
	...
    } while (cursor.moveToNext());
    cursor.close();
}
\end{minted}
\end{listing}